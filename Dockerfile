from ubuntu:latest
RUN sed -i 's/main$/main universe/' /etc/apt/sources.list \
	&& export DEBIAN_FRONTEND=noninteractive \
	 && apt-get update \
	 && apt-get upgrade -y \
	 && apt-get install -y \
      bc \
      bison \
      build-essential \
      ccache \
      curl \
      flex \
      g++-multilib \
      gcc-multilib \
      git \
      gnupg \
      gperf \
      imagemagick \
      lib32ncurses5-dev \
      lib32readline-dev \
      lib32z1-dev \
      liblz4-tool \
      libncurses5-dev \
      libsdl1.2-dev \
      libssl-dev \
      libwxgtk3.0-dev \
      libxml2 \
      libxml2-utils \
      lzop \
      pngcrush \
      rsync \
      schedtool \
      squashfs-tools \
      xsltproc \
      zip \
      zlib1g-dev \
      openjdk-8-jdk \
      android-tools-adb \
      android-tools-fastboot \
      bash-completion \
      bsdmainutils \
      file \
      nano \
      screen \
      sudo \
      tig \
      vim \
      wget \
      python \
      repo \
      adb \
      fastboot \
      unzip \
      coreutils \
      && rm -rf /var/lib/apt/lists/*
RUN mkdir /root/sdk
WORKDIR /root/sdk
RUN wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O sdk.zip\
      && unzip sdk.zip
RUN tools/bin/sdkmanager --update
RUN yes | tools/bin/sdkmanager tools sources\;android-28 platforms\;android-28 platform-tools patcher\;v4 extras\;m2repository\;com\;android\;support\;constraint\;constraint-layout\;1.0.2 extras\;m2repository\;com\;android\;support\;constraint\;constraint-layout-solver\;1.0.2 extras\;android\;m2repository build-tools\;28.0.3 
RUN yes | tools/bin/sdkmanager --licenses
ENV ANDROID_HOME /root/sdk
RUN     mkdir -p /root/android/saba
WORKDIR /root/android/saba
RUN 	repo init -u https://gitlab.com/bel-os/android.git -b saba-3
COPY    muppets.xml  /root/android/saba/.repo/local_manifests/muppets.xml
RUN	repo sync  --force-sync
